module.exports = {
    mode: "development",
    entry: "./src/js/game.js",  
    module: {
        rules: [{
            test: /\.s?css$/,
            use: [
                "style-loader", 
                "css-loader", 
                "sass-loader" 
            ]
        }],

        rules: [
            {
              test: /\.css$/,
              use: ['style-loader', 'css-loader']
            }
        ],

        rules: [{
            test: /\.(gif|jpg|png|jpe?g|svg)$/i,
            use: [
              'file-loader',
              {
                loader: 'image-webpack-loader',
                options: {
                  bypassOnDebug: true, 
                  disable: true, 
                },
              },
            ],
          }]
    },
}