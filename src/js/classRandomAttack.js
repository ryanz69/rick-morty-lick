import "./audio.js";
import { AIdamage } from "./classAIdamage.js";
import { Animation } from "./classAnimation.js";
import { Heal } from "./classHeal.js";
import { Psentence } from "./classP.js";
import { sound_attack } from "./audio.js";

export class randomAttack {
    constructor(skill_a, skill_b, skill_c, skill_d) {
        this.skill_a = skill_a;
        this.skill_b = skill_b;
        this.skill_c = skill_c;
        this.skill_d = skill_d;
    }

    randomAttack(player, computer) {
        let random = Math.floor(Math.random() * 4);
        let choice = [this.skill_a, this.skill_b, this.skill_c, this.skill_d];
        let attack = choice[random];
        if (random === 3) {
            if(attack.limitSort > 0) {
                attack.limitSort--;
                sound_attack(attack.sound);
            let heal = new Heal(computer.life, attack.damage, 200, "computerTarget");
            heal.funcHeal(player , computer);
            let p = new Psentence("Ordi: " + attack.sentence);
            p.Psentence(); 
            let anim = new Animation(attack.animate, attack.time, "player", attack.keyName);
            anim.animation();
            document.querySelector("#life_computer").textContent = computer.life;
            document.querySelector("#progressAI").value = computer.life;
            }
            else {
                let relance = new randomAttack(this.skill_a, this.skill_b, this.skill_c, this.skill_d);
                relance.randomAttack(player, computer);
            }
        }
        else {
            if(attack.limitSort > 0) {
                attack.limitSort--;
                sound_attack(attack.sound);
            let anim = new Animation(attack.animate, attack.time, "computer", attack.keyName);
            anim.animation();
            let degat = new AIdamage(player.life, attack.damage, attack.sentence);
            degat.funcAttackIA(player);
            }
            else {
                let relance = new randomAttack(this.skill_a, this.skill_b, this.skill_c, this.skill_d);
                relance.randomAttack(player, computer);
            }
        }
    }
}