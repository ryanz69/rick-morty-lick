import { Psentence } from "./classP.js";

export class AIdamage {
    constructor(life, damageLife, sentence) {
        this.life = life;
        this.damageLife = damageLife;
        this.sentence = sentence;
    }

    funcAttackIA(target) {
        target.life = this.life - this.damageLife;
        let p = new Psentence("Ordi: " + this.sentence);
        p.Psentence();
        document.querySelector("#life_player").textContent = target.life;
        document.querySelector("#progressPlayer").value = target.life;
    }
}