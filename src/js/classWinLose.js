import {win, lose} from "./affichageWinLose.js";
import { audio_win, audio_lose } from "./audio.js";
import { startEnd } from "./game.js";

export function winLose(player, computer) {

    if (player.life <= 0) {
        // Player = lose;
        lose();
        audio_lose();
        startEnd(false);
    }


    else if (computer.life <= 0) {
        // player = win;
        win();
        audio_win();
        startEnd(false);
    }



};


