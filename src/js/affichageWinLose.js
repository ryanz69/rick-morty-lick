export function win() {
    let last_width = document.querySelector("#cont_game").offsetWidth;
    let last_height = document.querySelector("#cont_game").offsetHeight;
    let div = document.createElement("div");
    let texte = document.createElement("h1");
    let remove = document.createElement("p");
    div.classList.add("page_pres", "d-flex", "justify-content-center", "align-items-center", "flex-column");
    texte.classList.add("d-flex", "m-auto");
    remove.classList.add("d-flex", "mb-auto");
    div.style.height = last_height + "px";
    div.style.width = last_width + "px";
    texte.textContent = "win";
    texte.setAttribute("id", "winLose");
    remove.textContent = "click here for restart game";
    remove.setAttribute("id", "remove");
    remove.addEventListener("click", function() {
        div.remove();
        location.reload();
    });
    document.querySelector("#cont_game").appendChild(div);
    div.appendChild(texte);
    div.appendChild(remove);
}

export function lose() {
    let last_width = document.querySelector("#cont_game").offsetWidth;
    let last_height = document.querySelector("#cont_game").offsetHeight;
    let div = document.createElement("div");
    let texte = document.createElement("h1");
    let remove = document.createElement("p");
    div.classList.add("page_pres", "d-flex", "justify-content-center", "align-items-center", "flex-column"); 
    texte.classList.add("d-flex", "m-auto");
    remove.classList.add("d-flex", "mb-auto");
    div.style.height = last_height + "px";
    div.style.width = last_width + "px";
    texte.textContent = "lose";
    texte.setAttribute("id", "winLose");
    remove.textContent = "click here for restart game";
    remove.setAttribute("id", "remove");
    remove.addEventListener("click", function() {
        div.remove();
        location.reload();
    });
    document.querySelector("#cont_game").appendChild(div);
    div.appendChild(texte);
    div.appendChild(remove);
}
