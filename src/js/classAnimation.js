export class Animation {
    constructor(image, time, type, name) {
        this.image = image;
        this.time = time;
        this.type = type;
        this.name = name;
    }

    animation() {
        let img = document.createElement("div");
        img.style.backgroundImage = "url("+this.image+")";
        img.classList.add("d-flex");
        img.classList.add("mx-auto");
        img.style.animationTimingFunction = "steps("+this.time+")";
        img.style.animationName = this.name;

        if(this.type === "player") {
            img.classList.add("computer");
            document.querySelector("#img_computer").appendChild(img);  
        }
        else if(this.type === "computer") {
            img.classList.add("player");
            document.querySelector("#img_player").appendChild(img);
        }
        
        setTimeout(function() {
           img.remove();
        }, 750);
    }
}