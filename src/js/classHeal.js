export class Heal {
    constructor(life, heal, maxLife, type) {
        this.life = life;
        this.heal = heal;
        this.maxLife = maxLife;
        this.type = type;
    }

    funcHeal(player, computer) {
        let lifeHeal = this.life + this.heal;

        if(lifeHeal > this.maxLife) {
            lifeHeal = this.maxLife;
        }

        if(this.type === "playerTarget") {
            player.life = lifeHeal;
        }

        else if(this.type === "computerTarget") {
            computer.life = lifeHeal;
        }
    }
}