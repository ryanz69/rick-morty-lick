export class Psentence {
    constructor(texte) {
        this.texte = texte;
    }

    Psentence() {
        let pA = document.querySelector("#pA").textContent;
        let pB = document.querySelector("#pB").textContent;
        let pC = document.querySelector("#pC").textContent;

        if (pA === "") {
            document.querySelector("#pA").textContent = this.texte;
        }

        else if (pB === "") {
            document.querySelector("#pB").textContent = this.texte;
        }

        else if (pC === "") {
            document.querySelector("#pC").textContent = this.texte;
        }

        else {

            document.querySelector("#pA").textContent = pB;
            document.querySelector("#pB").textContent = pC;
            document.querySelector("#pC").textContent = this.texte;

        }

    }
}