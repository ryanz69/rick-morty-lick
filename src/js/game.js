import { sound_attack, audio_slt, audio_attack } from "./audio.js";
import { Animation } from "./classAnimation.js";
import { Charactere } from "./classCharactere.js";
import { playerDamage } from "./classDamage.js";
import { Heal } from "./classHeal.js";
import { Psentence } from "./classP.js";
import { randomAttack } from "./classRandomAttack.js";
import { Spell } from "./classSpell.js";
import { winLose } from "./classWinLose.js";
import { selection } from "./selectionMap.js";

document.querySelector("#audio").addEventListener("click", function() {
    if(audio_slt.muted === true) {
        audio_slt.muted = false;
        audio_attack.muted = false;
        let icone = document.querySelector("#mute");
        icone.classList.remove("fa", "fa-volume-off");
        icone.classList.add("fa", "fa-volume-up");
    }
    else {
    audio_slt.muted = true;
    audio_attack.muted = true;
    let icone = document.querySelector("#mute");
    icone.classList.remove("fa", "fa-volume-up");
    icone.classList.add("fa", "fa-volume-off");
    }
});

// calcule taille dispo pour cont_log

let sort_size = document.querySelector("#cont_sort").offsetHeight;
let game_size = document.querySelector("#cont_game").offsetHeight;
let section_size = document.querySelector("section").offsetHeight;
let size = sort_size + game_size;
let dispo_size = section_size - size;

//prise longueur/largeur cont_game pour page_pres

let game_width = document.querySelector("#cont_game").offsetWidth;
let game_height = document.querySelector("#cont_game").offsetHeight;

window.onload = function () {
    document.querySelector("#page_pres").style.height = game_height + "px";
    document.querySelector("#page_pres").style.width = game_width + "px";
    document.querySelector("#cont_log").style.height = dispo_size + "px";

}

//changer les tailles quand la taille de l'ecran change

window.onresize = function () {
    let sort_size = document.querySelector("#cont_sort").offsetHeight;
    let game_size = document.querySelector("#cont_game").offsetHeight;
    let section_size = document.querySelector("section").offsetHeight;
    let game_width = document.querySelector("#cont_game").offsetWidth;
    let game_height = document.querySelector("#cont_game").offsetHeight;

    document.querySelector("#page_pres").style.height = game_height + "px";
    document.querySelector("#page_pres").style.width = game_width + "px";
    let size = sort_size + game_size;
    let dispo_size = section_size - size;
    document.querySelector("#cont_log").style.height = dispo_size + "px";
};

//remove page_pres au click

//variable verif debut game

let verif = false;

export function startEnd(value) {
    verif = value;
}

document.querySelector("#remove").addEventListener("click", function () {
    document.querySelector("#page_pres").remove();
    selection();
});

//CREATION JOUEUR

let player = new Charactere("morty", 200, "avatar");

document.querySelector("#progressPlayer").value = player.life;

//CREATION ORDI
let computer = new Charactere("pickleRick", 200, "avatar");

document.querySelector("#progressAI").value = computer.life;

//CREATION SORT JOUEUR

let sort_a = new Spell("attack", 15, "show me what you got", Infinity);
let sort_b = new Spell("slash", 25, "Existence is pain!", 5);
let sort_c = new Spell("strick", 35, "Nobody exists on purpose. Nobody belongs anywhere. We're all going to die. Come watch TV.", 2);
let sort_d = new Spell("heal", 50, "love me like you do", 4);

//CREATION SORT ORDI
let skill_a = new Spell("earth", 15, "I'm Pickle Riiiiick", Infinity);
skill_a.animate = "./css/Earth4.png";
skill_a.time = 5;
skill_a.keyName = "earth";
skill_a.sound = "./audio/Earth9.m4a";
let skill_b = new Spell("ice", 25, "It's time to get schwifty in here", 5);
skill_b.animate = "./css/Ice4.png";
skill_b.time = 4;
skill_b.keyName = "ice";
skill_b.sound = "./audio/Ice7.m4a";
let skill_c = new Spell("thunder", 35, "wubalubadubdub", 2);
skill_c.animate = "./css/Thunder3.png";
skill_c.time = 5;
skill_c.keyName = "thunder";
skill_c.sound = "./audio/Thunder11.m4a";
let skill_d = new Spell("heal", 50, "Weddings are basically funerals with cake.", 4);
skill_d.animate = "./css/HitPhoton.png";
skill_d.time = 4;
skill_d.keyName = "heal";
skill_d.sound = "./audio/Saint7.m4a";


//CREATION VARIABLE LIMIT SORT
let sortRestantA = sort_a.limitSort;
let sortRestantB = sort_b.limitSort;
let sortRestantC = sort_c.limitSort;
let sortRestantD = sort_d.limitSort;

//AFFICHAGE STATS/NOM SORTS 
document.querySelector("#life_player").textContent = player.life;
document.querySelector("#name_player").textContent = player.name;
document.querySelector("#life_computer").textContent = computer.life;
document.querySelector("#name_computer").textContent = computer.name;
document.querySelector("#nameA").textContent = sort_a.name;
document.querySelector("#nameB").textContent = sort_b.name;
document.querySelector("#nameC").textContent = sort_c.name;
document.querySelector("#nameD").textContent = sort_d.name;
document.querySelector("#limitA").textContent = "\u221e/\u221e";
document.querySelector("#limitB").textContent = sortRestantB + "/" + sort_b.limitSort;
document.querySelector("#limitC").textContent = sortRestantC + "/" + sort_c.limitSort;
document.querySelector("#limitD").textContent = sortRestantD + "/" + sort_d.limitSort;


//TIMER ENTRE CLIQUE
let timer = true;

function time() {
    timer = false;
    document.querySelectorAll(".sortBlock").forEach(function(e) {e.style.cursor = "wait"})
    setTimeout(function () {
        timer = true;
        document.querySelectorAll(".sortBlock").forEach(function(e) {e.style.cursor = "pointer"})
    }, 1600);
}

//ADDEVENT CLICK SORT

document.querySelector("#sort_a").addEventListener("click", function () {
    if (verif === true) {
        if (timer === true) {
            if (sortRestantA > 0) {
                sortRestantA--;
                sound_attack("./audio/Sword6.m4a");
                let degat = new playerDamage(computer.life, sort_a.damage, sort_a.sentence);
                degat.funcAttack(computer);
                let anim = new Animation("./css/Hit1.png", 4, "player", "hitA");
                anim.animation();
                winLose(player, computer);               
                if(verif === true) {
                    setTimeout(function () {
                        let random = new randomAttack(skill_a, skill_b, skill_c, skill_d);
                        random.randomAttack(player, computer);
                        winLose(player, computer);
                    }, 800);
                time();
                }

            }
            else {
                let p = new Psentence("Sort non disponible");
                p.Psentence();
            }
        }
    }
});

document.querySelector("#sort_b").addEventListener("click", function () {
    if (verif === true) {
        if (sortRestantB > 0) {
            if (timer === true) {
                sortRestantB--;
                sound_attack("./audio/Slash13.m4a");
                document.querySelector("#limitB").textContent = sortRestantB + "/" + sort_b.limitSort;
                let degat = new playerDamage(computer.life, sort_b.damage, sort_b.sentence);
                degat.funcAttack(computer);
                let anim = new Animation("./css/Slash.png", 5, "player", "slash");
                anim.animation();
                winLose(player, computer);
                if(verif === true) {
                    setTimeout(function () {
                        let random = new randomAttack(skill_a, skill_b, skill_c, skill_d);
                        random.randomAttack(player, computer);
                        winLose(player, computer);
                    }, 800);
                time();
                }
            }
            else {
                let p = new Psentence("Sort non disponible");
                p.Psentence();
            }
        }
    }
});

document.querySelector("#sort_c").addEventListener("click", function () {
    if (verif === true) {
        if (timer === true) {
            if (sortRestantC > 0) {
                sortRestantC--;
                sound_attack("./audio/Explosion3.m4a");
                document.querySelector("#limitC").textContent = sortRestantC + "/" + sort_c.limitSort;
                let degat = new playerDamage(computer.life, sort_c.damage, sort_c.sentence);
                degat.funcAttack(computer);
                let anim = new Animation("./css/Hit2.png", 3, "player", "hitB");
                anim.animation();
                winLose(player, computer);
                if(verif === true) {
                    setTimeout(function () {
                        let random = new randomAttack(skill_a, skill_b, skill_c, skill_d);
                        random.randomAttack(player, computer);
                        winLose(player, computer);
                    }, 800);
                time();
                }
            }
            else {
                let p = new Psentence("Sort non disponible");
                p.Psentence();
            }
        }
    }
});

document.querySelector("#sort_d").addEventListener("click", function () {
    if (verif === true) {
        if (timer === true) {
            if (sortRestantD > 0) {
                sortRestantD--;
                sound_attack("./audio/Saint7.m4a");
                document.querySelector("#limitD").textContent = sortRestantD + "/" + sort_d.limitSort;
                let heal = new Heal(player.life, sort_d.damage, 200, "playerTarget");
                heal.funcHeal(player, computer);
                let p = new Psentence("Player: " + sort_d.sentence);
                p.Psentence();
                document.querySelector("#life_player").textContent = player.life;
                document.querySelector("#progressPlayer").value = player.life;
                let anim = new Animation("./css/HitPhoton.png", 5, "computer", "heal");
                anim.animation();
                winLose(player, computer);
                if(verif === true) {
                    setTimeout(function () {
                        let random = new randomAttack(skill_a, skill_b, skill_c, skill_d);
                        random.randomAttack(player, computer);
                        winLose(player, computer);
                    }, 800);
                time();
                }
            }
            else {
                let p = new Psentence("Sort non disponible");
                p.Psentence();
            }
        }
    }
});


